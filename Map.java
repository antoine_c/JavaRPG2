import java.util.Random;
import java.util.Scanner;


public class Map 
{
	private int i;
	private int j;
	private int maxI=11;
	private int maxJ=10;
	String map[][]= //Map de base ; La map active sera s�lectionn�e al�a entre plusieurs mod�les
		   {{"#","#","#","#","#","#","#","#","#","#"},
		    {"#"," "," ","p"," "," ","a"," "," ","#"},
            {"#"," "," "," ","#"," "," ","m"," ","#"},
	        {"#"," "," "," ","p"," "," ","a"," ","#"},
		    {"#"," "," "," "," "," "," "," "," ","#"},
			{"#"," ","#"," "," "," "," ","p"," ","#"},
			{"#"," ","m"," "," "," "," "," "," ","#"},
			{"#"," "," "," "," "," "," "," ","#","#"},
			{"#"," "," "," "," ","a"," "," "," ","#"},
			{"#"," "," "," "," ","J"," "," "," ","#"},
			{"#","#","#","#","#","#","#","#","#","#"}};;
	
	
	public void setMap(String[][] map, int maxI, int maxJ)
	{
		this.map=map;
		this.maxI=maxI;
		this.maxJ=maxJ;
	}
	public void mapVide()
	{
		
	}
	public void positionJoueur()//Affecte a map.i & map.j les case du tableau ou le joueur J est plac�
	{
		for(int k=0;k<11;k++)
		{
			for(int l=0;l<10;l++)
			{
				if(this.map[k][l]=="J")
				{
					this.i=k;
					this.j=l;
				}
			}
		}
	}
	
	public void checkObstacle(PJ joueur, Sac sac) throws InterruptedException //V�rifie ce qu'il y a autour du Joueur, et d�termine la fonction a utiliser
	{
		positionJoueur(); //Appel mouvement si case adj vide, sinon : combat(m) sac(p/a/bouclier?)
		boolean coord=false;
		int tempI=this.i; //Coord de destination
		int tempJ=this.j;
		int iniI=this.i; //Coord de base
		int iniJ=this.j;
		while(coord==false) 
		{
			Scanner sc = new Scanner(System.in);
			String repTemp=sc.nextLine();
			char reponse =repTemp.charAt(0);
			if(reponse=='z')
			{
				tempI=this.i-1;
			}
			else if(reponse=='q')
			{
				tempJ=this.j-1;
			}
			else if(reponse=='s')
			{
				tempI=this.i+1;
			}
			else if(reponse=='d')
			{
				tempJ=this.j+1;
			}
			else if(reponse=='b') //Inventaire
			{
				sac.gererSac(joueur);
				coord=true;
			}
			else if(reponse=='c')
			{
				joueur.stats(sac,joueur);
				coord=true;
			}
			
			if(this.map[tempI][tempJ]=="#") //Hors map
				{
					System.out.println("Vous ne pouvez pas vous deplacer hors de la carte");
				}
			
			else if(this.map[tempI][tempJ]==" ") //Deplacement autoris�
				{
					this.mouvement(iniI,iniJ,tempI,tempJ); //Effectue le mouvement avec les coordonnees d�termin�es par l'entr�e clavier
					coord=true;
				}
			
			else if(this.map[tempI][tempJ]=="a") //Arme
			{
				Random rand = new Random(); //Generation d'une arme al�atoire
				int nbAlea = rand.nextInt(4 + 1);
				Objets arme1 = new Objets("temp",0,0,0,false,false);
				if(nbAlea==0)
				{
					arme1.setNom("Tranche Entraille");
					arme1.setDegats(3);
					arme1.setDefense(1);
				}
				else if(nbAlea==1)
				{
					arme1.setNom("Acier �mouss�");
					arme1.setDegats(1);
					arme1.setDefense(0);
				}
				else if(nbAlea==2)
				{
					arme1.setNom("Hache de la mort");
					arme1.setDegats(6);
					arme1.setDefense(0);
				}
				else if(nbAlea==3)
				{
					arme1.setNom("Ep�e equilibr�e");
					arme1.setDegats(5);
					arme1.setDefense(2);
				}
				else if(nbAlea==4)
				{
					arme1.setNom("Dague microscopique");
					arme1.setDegats(3);
					arme1.setDefense(0);
				}
				sac.ajouterSac(arme1);
				System.out.println("Vous avez r�cup�r� : " + arme1.getNom() + " --- Attaque:" + arme1.getDegats() + " Defense:" + arme1.getDefense() + " Soins:" + arme1.getSoins());
				sac.afficherSac(joueur);
				this.mouvement(iniI,iniJ,tempI,tempJ);
				coord=true;
			}
			
			else if(this.map[tempI][tempJ]=="p")
			{
				Random rand = new Random(); //Generation d'une potion al�atoire
				int nbAlea = rand.nextInt(3 + 1); //4 possibilit�es
				Objets potion1 = new Objets("temp",0,0,0,true,false); //true->unique false->non equipe
				System.out.println("Generation al�atoire -> " + nbAlea);		
				if(nbAlea==0)
				{
					potion1.setNom("Senzu");
					potion1.setSoins(5);
				}
				else if(nbAlea==1)
				{
					potion1.setNom("Bombe Lacrymog�ne");
					potion1.setDegats(3);
				}
				else if(nbAlea==2)
				{
					potion1.setNom("Pommade bon march�");
					potion1.setSoins(2);
				}
				else if(nbAlea==3)
				{
					potion1.setNom("Grenade a fragmentation");
					potion1.setDegats(3);
				}
				sac.ajouterSac(potion1);
				System.out.println("Vous avez r�cup�r� : " + potion1.getNom() + " --- Attaque:" + potion1.getDegats() + " Defense:" + potion1.getDefense() + " Soins:" + potion1.getSoins());
				sac.afficherSac(joueur);
				this.mouvement(iniI,iniJ,tempI,tempJ);
				coord=true;
			}
			else if(this.map[tempI][tempJ]=="m")
			{
				Random rand = new Random(); //Generation d'une potion al�atoire
				int nbAlea = rand.nextInt(1 + 1); //2 possibilit�es
				PNJ mob1 = new PNJ("temp",true,0,0,0,true); //true->vivant 0->degats 0->defense 0->blessure true->hostile
				if(nbAlea==0)
				{
					mob1.setNom("Orc L�murien");
					mob1.setDegats(1);
					mob1.setDefense(1);
				}
				else if(nbAlea==1)
				{
					mob1.setNom("G�ant des montagnes");
					mob1.setDegats(3);
					mob1.setDefense(2);
				}
				System.out.println("Vous rencontrez un " + mob1.getNom());
				System.out.println("Il vous menace sauvagement avec un gourdin et vous attaque !");
				joueur.combat(joueur,mob1,sac);
				this.mouvement(iniI,iniJ,tempI,tempJ);
				coord=true;
			}
		}
	}
	
	public void mouvement(int iniI, int iniJ, int tempI, int tempJ) //effectue le mouvement en this.map[tempI][tempJ] et remplace l'ancinne position par un espace
	{
		this.map[iniI][iniJ]=" ";
		this.map[tempI][tempJ]="J";
	}
	
	public String afficheMap() //Affiche une map 11x10
	{
		String result="";
		for(int i=0;i<this.maxI;i++)
		{
			for(int j=0;j<this.maxJ;j++)
			{
				result=result + this.map[i][j];
			}
			result=result+ "\n";
		}
		return result;
	}
}

