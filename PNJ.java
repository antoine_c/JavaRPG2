
public class PNJ 
{
	private String nom;
	private boolean vivant;
	private int degats;
	private int defense;
	private int blessure;
	private boolean hostile;
	
	public PNJ(String nom, boolean vivant, int degats, int defense, int blessure, boolean hostile)
	{
		this.nom=nom;
		this.vivant=vivant;
		this.degats=degats;
		this.defense=defense;
		this.blessure=blessure;
		this.hostile=hostile;
	}
	public String getNom()
	{
		return this.nom;
	}
	public boolean getVivant()
	{
		return this.vivant;
	}
	public int getDegats()
	{
		return this.degats;
	}
	public int getDefense()
	{
		return this.defense;
	}
	public int getBlessure()
	{
		return this.blessure;
	}
	public boolean getHostile()
	{
		return this.hostile;
	}
	public void setNom(String nom)
	{
		this.nom=nom;
	}
	public void setVivant(boolean vivant)
	{
		this.vivant=vivant;
	}
	public void setDegats(int degats)
	{
		this.degats=degats;
	}
	public void setDefense(int defense)
	{
		this.defense=defense;
	}
	public void setBlessure(int blessure)
	{
		this.blessure=blessure;
	}
	public void setHostile(boolean hostile)
	{
		this.hostile=hostile;
	}
	public void checkBlessure()
	{
		if(this.blessure>=5)
		{
			this.setVivant(false);
		}
	}
}
