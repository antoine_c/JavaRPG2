import java.util.Random;
import java.util.Scanner;


/*Cette classe comprend :
 * Les caract�ristiques du PJ (Blessure,Experience,F.A.R,Etat)
 * Fonctions setXp,getXp--setBlessure,getBlessure--setVivant,getVivant
 * Contiendra ?
 * Interaction inventaire
 * Attaquer
 * Se deplacer
 * Defense
 * Envoyer message
 */
public class PJ 
{
	private boolean vivant;
	private int force;
	private int adresse;
	private int resistance;
	private int xp;
	private int niveau;
	private int blessure;
	private int ptsAction;
	private boolean mainPrise;

	
	public PJ(boolean vivant, int force, int adresse, int resistance, int xp, int niveau, int blessure, int ptsAction)
	{
		this.vivant=vivant;
		this.adresse=adresse;
		this.resistance=resistance;
		this.xp=xp;
		this.niveau=niveau;
		this.blessure=blessure;
		this.ptsAction=ptsAction;
	}
	
	public void setMainPrise(boolean mainPrise)
	{
		this.mainPrise=mainPrise;
	}
	public boolean getMainPrise()
	{
		return this.mainPrise;
	}
   public int getXp()
   {
	return xp;
   }
   
   public void setXp(int xp) //Monstre tu�, Combat gagn� ...
   {
	this.xp= xp;   
   }
   
   public void checkXp()//V�rifie si le joueur gagne un niveau ou non
   {
	   if(getXp()>=(niveau*10))
	   {
		   levelUp(force,adresse,resistance);
		   System.out.println("Vous gagnez un niveau !!\nNiveau actuel : " + this.getNiveau() +"\n");
	   }
   }
   
   public void setForce(int force)
   {
	   this.force=force;
   }
   public int getForce()
   {
	   return force;
   }
   public void setAdresse(int adresse)
   {
	   this.adresse=adresse;
   }
   public int getAdresse()
   {
	   return adresse;
   }
   public void setResistance(int resistance)
   {
	   this.resistance=resistance;
   }
   public int getResistance()
   {
	   return resistance;
   }
   public int getNiveau()
   {
	   return this.niveau;
   }
   
   public void levelUp(int force, int adresse, int resistance)//Augmente les caract�ristiques du joueur
   {
	   this.force++;
	   this.adresse++;
	   this.resistance++;
	   this.niveau++;
   }
   
   public int getBlessure() //V�rifie la sant�
   {
	   return blessure;
   }
   
   public void setBlessure(int blessure) //Inflige ou soigne la sant�
   {
	   this.blessure= blessure;
   }
   
   public void checkBlessure()//V�rifie que le joueur n'est pas mort suite au combat
   {
	   if(this.getBlessure()>=5)
	   {
		   this.setVivant(false);
	   }
   }
   
   public void setVivant(boolean vivant) //Donne la mort & la vie
   {
	   this.vivant=vivant;
   }
   
   public boolean getVivant() //Verifie l'�tat du corps
   {
	   return vivant;
   }
   
      
   public void stats(Sac sac, PJ joueur1)
   {
	   System.out.println("Joueur 1 \n Vos objets �quip�s : %s\n");
	   //PJ.afficherObj();
	   System.out.println("Votre niveau de blessure : " + blessure);
	   if (blessure==0)
		{
		   	System.out.println("Vous �tes en pleine forme");
		}
	   else if (blessure==1)
		{
		   	System.out.println("Vous �tes �souffl�s");
		}
	   else if (blessure==2)
		{
		   	System.out.println("Vous avez pris une fl�che dans le genou");
		}
	   else if (blessure==3)
		{
		   	System.out.println("Vous �tes s�veremment touch�s");
		}
	   else
	   {
		   System.out.println("Vous �tes inconscient");
	   }
	   System.out.println("Vos points d'action : " +ptsAction);
	   System.out.println("Vos caract�ristiques : force:" +this.force+ " adresse:" +this.adresse+ " resistance:" + this.resistance);
	   System.out.println("Votre niveau : " + getNiveau());
   }
   public void combat(PJ joueur1, PNJ mob1, Sac sac) throws InterruptedException
   {
	   boolean combatOver=false;
	   int positionPotion=0;
	   int attaqueJoueur=joueur1.getForce() - mob1.getDefense();
	   int attaqueMob=mob1.getDegats() - joueur1.getResistance();
	   if(attaqueJoueur<=0) //Si l'attaque deviens n�gative (si la resistance du defenseur est plus grande que les degats de l'attaquan) elle est fix�e � 1
	   {
		   attaqueJoueur=1;
	   }
	   if(attaqueMob<=0)
	   {
		   attaqueMob=1;
	   }
	   System.out.println("\nVous attaquez en premier ..");
	   Scanner sleep = new Scanner(System.in);
	   Thread.sleep(1000); //permet de fluidifier l'affichage
	   if(sac.potionCombat(positionPotion)==true) //Utilisation d'une potion de degats si possible
	   {
		   System.out.println("Voulez vous utiliser une potion contre " + mob1.getNom() + " ?");
		   Scanner sc = new Scanner(System.in);
		   String repTemp=sc.nextLine();
		   char reponse =repTemp.charAt(0);
		   if(reponse=='y')
		   {
			   sac.utiliserPotion(positionPotion,mob1);
		   }
	   }
	   while(combatOver==false)
	   {
		   Random randJ = new Random(); //Generation de degats suppl�mentaire
		   int nbAleaJ = randJ.nextInt(1 + 2);
		   attaqueJoueur=attaqueJoueur+nbAleaJ;
		   mob1.setBlessure(mob1.getBlessure() + attaqueJoueur);
		   System.out.println("\nVous infligez " + attaqueJoueur +" a " + mob1.getNom());
		   System.out.println("Son niveau de blessure : " +mob1.getBlessure());
		   Thread.sleep(3000);
		   mob1.checkBlessure(); //Verifie que le mob est mort ou non
		   attaqueJoueur=attaqueJoueur+nbAleaJ;
		   if(mob1.getVivant()==false)
		   {
			   System.out.println("\nVous avez tu� " + mob1.getNom());
			   int XpWin=attaqueMob+10;
			   System.out.println("Vous gagnez " + XpWin +" d'�xperience\n");
			   joueur1.setXp(getXp()+XpWin);
			   joueur1.checkXp(); //Verifie si le joueur gagne un niveau ou non
			   combatOver=true;
		   }
		   else
		   {
			   Random randM = new Random(); //Generation de degats suppl�mentaire
			   int nbAleaM = randM.nextInt(1 + 2);
			   attaqueMob=attaqueMob+nbAleaM;
			   System.out.println("\n" + mob1.getNom() + " attaque a son tour !");
			   joueur1.setBlessure(joueur1.getBlessure() + attaqueMob);
			   System.out.println("Il vous inflige " + attaqueMob);
			   System.out.println("Votre niveau de blessure : " +joueur1.getBlessure());
			   Thread.sleep(3000);
			   joueur1.checkBlessure();
			   attaqueMob=attaqueMob-nbAleaM;
			   if(joueur1.getVivant()==false)
			   {
				   System.out.println("Vous etes mort !");
				   combatOver=true;
			   }
		   }
	   }
   }
}
