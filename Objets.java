/*Un objet s'équipe/desequipe quand l'utilisateur le choisis dans le sac a dos
 * Cela lui attribu des caractéristiques supplémentaires
 * L'utilisation d'un objet unique requiert ensuite sa destruction
 */

public class Objets 
{
	private String nom;
	private int degats;
	private int defense;
	private int soins;
	private boolean unique; //Sans durabilitée si false, sinon true (potion)
	private boolean equipe; //Objet equipé ou non
	
	public Objets(String nom, int degats, int defense, int soins, boolean unique, boolean equipe)
	{
		this.nom=nom;
		this.degats=degats;
		this.defense=defense;
		this.soins=soins;
		this.unique=unique;
		this.equipe=equipe;
	}
	public void setNom(String nom)
	{
		this.nom=nom;
	}
	public void setDegats(int degats)
	{
		this.degats=degats;
	}
	public void setDefense(int defense)
	{
		this.defense=defense;
	}
	public void setSoins(int soins)
	{
		this.soins=soins;;
	}
	public void setUnique(boolean unique)
	{
		this.unique=unique;
	}
	public String getNom()
	{
		return this.nom;
	}
	public int getDegats()
	{
		return this.degats;
	}
	public int getDefense()
	{
		return this.defense;
	}
	public int getSoins()
	{
		return this.soins;
	}
	public boolean getUnique()
	{
		return this.unique;
	}
	
	public boolean getEquipe()
	{
		return this.equipe;
	}
	
	public void setEquipe(boolean equipe)
	{
		this.equipe=equipe;
	}
	
	public void equiper()//Affecte l'objet ciblé au sac, aux stats, 
	{
		
	}
	
	public void desequiper()
	{
		
	}
	
	public void utiliser()
	{
		//objet=NULL;
	}
}
