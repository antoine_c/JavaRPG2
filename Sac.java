import java.util.ArrayList;
import java.util.Scanner;

public class Sac 
{
	private ArrayList<Objets> item = new ArrayList<Objets>();
	
	public void ajouterSac(Objets o)//Place l'objet apr�s le dernier objet stock�
	{
		item.add(o);
	}
	
	public boolean afficherSac(PJ joueur1)
	{
		boolean vide;
		System.out.println("Vous consultez votre sac ...");
		if(item==null || item.isEmpty()==true) //Si le sac est vide
		{
			System.out.println("Votre sac est vide ! :)");
			vide=true;;
		}
		else //Si le sac n'est pas vide
		{
			for(int i=0;i<item.size();i++) //Parcours du sac
			{
				String s = item.get(i).getNom() + "(" + item.get(i).getDegats() +"/" + item.get(i).getDefense() +"/" + item.get(i).getSoins() + ")"; //Liste des objets
				System.out.println(i+1 + " - " +s);
			}
			vide=false;
		}
		return vide;
	}
	
	public boolean potionCombat(int positionPotion)//Retourne true si il y a une potion degats dans le sac, et affecte sa position au parametre
	{
		boolean potion=false;
		for(int i=0;i<item.size();i++)
		{
			if((this.item.get(i).getSoins()<=0) && (this.item.get(i).getDegats()>0) && (this.item.get(i).getUnique()==true))//Si l'objet est unique et qu'il inflige des degats sans soigner
			{
				potion=true;
				positionPotion=i;
			}
		}
		if(potion==false)
		{
			System.out.println("Il n'y a pas de popo degats dans votre sac");
			potion=false;
		}
		return potion;
	}
	
	public void utiliserPotion(int positionPotion, PNJ mob1)
	{
		int degatsInflige;
		positionPotion++;
		System.out.println("Vous utilisez " + item.get(positionPotion).getNom());
		degatsInflige=item.get(positionPotion).getDegats();
		mob1.setBlessure(degatsInflige);
		System.out.println("Vous avez inflig� " + degatsInflige +" de degats � " + mob1.getNom());
	}
	
	public void gererSac(PJ joueur1)
	{
		boolean tempArme=false;
		boolean tempPotion=false;
		boolean setBug=false;
		int positionTemp=0;
		if(afficherSac(joueur1)==false) //Si le sac n'est pas vide
		{
			for(int i=0;i<item.size();i++) //Parcours du sac
			{
				if((item.get(i).getDegats()>0) && (joueur1.getMainPrise()==false) && (item.get(i).getUnique()==false)) // Si l'objet inflige des degats, que ce n'est pas une potion et que le joueur n'a pas d'arme �quip�e
				{
					tempArme=true;
				}
				if(item.get(i).getSoins()>0) //SI l'objet peut soigner
				{
					tempPotion=true;
				}
				if(item.get(i).getEquipe()==true)
				{
					positionTemp=i; //Stocke l'indice ou l'arme �quip�e est plac�e, �vite de demander l'indice pour d�s�quiper
				}
			}
		}
		if(tempArme==true)
		{
			System.out.println("Voulez vous equiper une arme ? (y/n) ");
			Scanner sc = new Scanner(System.in);
			String repTemp=sc.nextLine();
			char reponse =repTemp.charAt(0);
			boolean val=false;
			while(val==false) //Boucle qui oblige une r�ponse 
			{
				if(reponse=='y')
				{
					System.out.println("Quel est son emplacement ?");
					Scanner sd = new Scanner(System.in);
					int position=sd.nextInt();
					if(item.get(position-1).getDegats()>0 && item.get(position-1).getUnique()==false)
					{
						joueur1.setForce( joueur1.getForce()+ item.get(position-1).getDegats()); //Augmente la force du joueur des statistiques de l'arme
						joueur1.setResistance(joueur1.getResistance()+item.get(position-1).getDefense());
						item.get(position-1).setEquipe(true);
						joueur1.setMainPrise(true);
						System.out.println(item.get(position-1).getNom() + " est �quip� !");
						setBug=true; //Evite de demander apr�s avoir �quip� l'arme, de la d�s�quiper dans le prochain if
						val=true;
					}
					else
					{
						System.out.println("Ce n'est pas une arme");
					}
				}
				else if(reponse=='n')
				{
					val=true;
				}
				else
				{
					System.out.println("Ce n'est pas une r�ponse valide");
				}
			}
		}
		if(joueur1.getMainPrise()==true && setBug==false) //Si le joueur poss�de une arme �quip�e
		{
			System.out.println("Voulez vous d�s�quiper votre arme ? (y/n)");
			Scanner sc = new Scanner(System.in);
			String repTemp=sc.nextLine();
			char reponse =repTemp.charAt(0);
			if(reponse=='y')
			{
				joueur1.setForce( joueur1.getForce() - item.get(positionTemp).getDegats());
				joueur1.setResistance( joueur1.getResistance() - item.get(positionTemp).getDefense());
				joueur1.setMainPrise(false);
				item.get(positionTemp).setEquipe(false);
				System.out.println("Votre arme est d�s�quip�e et reste dans votre sac");
			}
		}
		if(tempPotion==true) //Si un objet de l'inventaire peut soigner
		{
			System.out.println("Voulez vous utiliser une potion de soins ? (y/n)");
			Scanner sf = new Scanner(System.in);
			String repTemp=sf.nextLine();
			char reponse =repTemp.charAt(0);
			if(reponse=='y')
			{
				System.out.println("Quel est son emplacement ?");
				Scanner sd = new Scanner(System.in);
				int position=sd.nextInt();
				int soigneTemp=item.get(position-1).getSoins(); //Le soins est �gal � la valeur soins de l'objet potion
				if(soigneTemp>=joueur1.getBlessure()) //Si le soins effectu� donne une valeur sup�rieure � la vie du joueur
				{
					joueur1.setBlessure(0);
					System.out.println("Vous �tes totalement soign�s");
				}
				else
				{
					joueur1.setBlessure(joueur1.getBlessure()-soigneTemp);
					System.out.println("La potion fait de l'effet et vous soigne de " + soigneTemp);
				}
			}
		}
	}
}
